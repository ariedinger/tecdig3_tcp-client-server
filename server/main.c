#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

/* Arbitrary, but client and server must match */
#define SERVER_PORT 8080
/* Block size for transfer */
#define BUF_SIZE 4096
/* Size of the queue */
#define QUEUE_SIZE 10


int main(int argc, char *argv[])
{
   int s, b, l, fd, sa, bytes, on = 1;
   char buf[BUF_SIZE];
   struct sockaddr_in channel;


   /* Build the address structure to bind the socket */
   memset(&channel, 0, sizeof(channel));
   channel.sin_family = AF_INET;
   channel.sin_addr.s_addr = htonl(INADDR_ANY);
   channel.sin_port = htons(SERVER_PORT);

   /* Create the socket */
   s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

   if (s < 0)
   {
      printf("socket failed\n");
      exit(1);
   }

   setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));

   b = bind(s, (struct sockaddr *) &channel, sizeof(channel));

   if (b < 0)
   {
      printf("bind failed\n");
      exit(1);
   }

   /* Specify the size of the queue */
   l = listen(s, QUEUE_SIZE);
   if (l < 0)
   {
      printf("listen failed\n");
      exit(1);
   }


   /* The socket is now configured and bound. Wait for a connection and process it. */
   while (1)
   {
      /* Block for connection request */
      sa = accept(s, 0, 0);
      if (sa < 0)
      {
         printf("accept failed\n");
         exit(1);
      }

      /* Read file name from the socket */
      read(sa, buf, BUF_SIZE);

      /* Get and return the file. */
      fd = open(buf, O_RDONLY);
      if (fd < 0)
      {
         printf("open failed\n");
         exit(1);
      }

      while (1)
      {
          /* Read from file */
          bytes = read(fd, buf, BUF_SIZE);

          /* Check for end of file */
          if (bytes <= 0) break;

          /* Write bytes to the socket */
          write(sa, buf, bytes);
      }

      /* Close the file */
      close(fd);

      /* Close the connection */
      close(sa);
   }
}
