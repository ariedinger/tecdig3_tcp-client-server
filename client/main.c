#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>

/* Arbitrary, but the client and server must match */
#define SERVER_PORT 8080
/* Block size for transfer */
#define BUF_SIZE 4096

int main(int argc, char **argv)
{
   int c, s, bytes;
   /* Buffer for incoming file */
   char buf[BUF_SIZE];
   /* Server information */
   struct hostent *h;
   /* Contains the IP address */
   struct sockaddr_in channel;

   FILE *fichero;

   if (argc != 3)
   {
      printf("Usage: client server-name file-name\n");
      exit(1);
   }

   /* Look up the IP address of the host */
   h = gethostbyname(argv[1]);

   if (!h)
   {
      printf("gethostbyname failed\n");
      exit(1);
   }

   s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (s < 0)
   {
      printf("socket");
      exit(1);
   }

   memset(&channel, 0, sizeof(channel));
   channel.sin_family = AF_INET;
   memcpy(&channel.sin_addr.s_addr, h->h_addr, h->h_length);
   channel.sin_port = htons(SERVER_PORT);

   c = connect(s, (struct sockaddr *) &channel, sizeof(channel));
   if (c < 0)
   {
      printf("Connection failed\n");
      exit(1);
   }

   fichero = fopen(argv[2],"w+");

   if(fichero==NULL)
   {
      printf("Cannot open or create the file\n");
   }

   /* Connection established. Send the file name including the terminating byte 0. */
   write(s, argv[2], strlen(argv[2])+1);
   fwrite(buf,4096,4096,fichero);

   /* Get the file and write it to the standard output. */
   while (1)
   {
      /* Read from the socket */
      bytes = read(s, buf, BUF_SIZE);
      if (bytes <= 0)
         /* Check for end of file */
         exit(0);

      /* Write to the standard output */
      write(1, buf, bytes);
      fwrite(buf,4096,4096,fichero);
   }

   fclose(fichero);
}

void fatal(char *string)
{
   printf("%s\n", string);
   exit(1);
}
